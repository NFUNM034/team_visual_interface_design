# team_visual_interface_design

## 一、竞品分析：网易云音乐

### 1.1架构层

 **主导航的信息架构简要列明：** 

![主导航的信息架构简要列明](https://images.gitee.com/uploads/images/2020/0511/210145_10b54602_1922224.png "1.1_1.png")

 **细分页面的结构框架：** 

![细分页面的结构框架](https://images.gitee.com/uploads/images/2020/0511/210203_df829c25_1922224.png "1.1_2.png")

总结：网易云音乐的功能定位主要为音乐的收听、发现和分享；内容上主要有歌曲、歌单、电台、视频、直播等分类；音乐专题较多样，能够吸引各种年轻用户，但功能稍微有点繁杂，不够简单。

### 1.2交互路径分析

 **交互路径分析：** 

![交互路径分析](https://images.gitee.com/uploads/images/2020/0511/210313_c1baca43_1922224.png "1.2_1.png")

总结：网易云音乐的播放界面的交互路径功能齐全，但也显得页面略复杂。

 **交互亮点提取：** 

![交互亮点提取](https://images.gitee.com/uploads/images/2020/0511/210411_791fc3a4_1922224.png "1.2_2.png")


### 1.3视觉层面分析
![视觉层面分析](https://images.gitee.com/uploads/images/2020/0511/210457_2d192431_1922224.jpeg "1.3.jpeg")


## 二、自己作品：随听APP

### 2.1结构层

![结构层](https://images.gitee.com/uploads/images/2020/0511/210615_883f599a_1922224.png "2.1.png")


### 2.2交互路径

![交互路径](https://images.gitee.com/uploads/images/2020/0511/211436_9e56b566_1922224.png "2.2.png")
![交互亮点提取](https://images.gitee.com/uploads/images/2020/0511/213900_3b7330c7_1648172.png "屏幕截图.png")
### 2.3视觉规范

![视觉规范1](https://images.gitee.com/uploads/images/2020/0511/210636_b2e60e58_1922224.jpeg "2.3_1.jpeg")

![视觉规范2](https://images.gitee.com/uploads/images/2020/0511/210706_f929ae3d_1922224.jpeg "2.3_2.jpg")